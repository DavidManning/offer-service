## Offers Service

This Spring Boot Java application presents a REST API for accepting and retrieving merchant offers.

## Building
Build with maven using: mvn package

This will produce a runnable .jar named merchantoffer-x.y-SNAPSHOT.jar

## Running
The runnable .jar can be run from the command line:

java -jar target/merchantoffer-1.0-SNAPSHOT.jar

## Assumptions
- Money amount will be to 2dp always. This should cater for the majority of world currencies
- The app should be runnable stand-alone (the instructions above detail how to run it). The app can be run inside an application container like Tomcat or Jetty with minor tweaks




