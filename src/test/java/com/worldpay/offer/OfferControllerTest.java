package com.worldpay.offer;

import com.google.common.collect.ImmutableList;
import com.worldpay.offer.domain.Condition;
import com.worldpay.offer.domain.Money;
import com.worldpay.offer.domain.Offer;
import com.worldpay.offer.domain.OfferRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static java.util.Currency.getInstance;
import static org.hamcrest.CoreMatchers.is;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest
public class OfferControllerTest {

    @Autowired
    private MockMvc mock;

    @MockBean
    private OfferRepository offerRepository;

    @Test
    public void testPostNewOffer() throws Exception {
        // Given the repository persists and returns an Offer with an auto-generated id
        when(offerRepository.save(any(Offer.class)))
                .thenReturn(new Offer() {{setId(456);}});

        String offerBody = "{\"description\": \"Test Offer\",\"price\":{\"amount\":123.45678,\"currency\":\"GBP\"," +
                           "\"conditions\": [\"cond1\"]}}";

        // When we POST a new Offer
        mock.perform(post("/offer").content(offerBody)
                .accept(APPLICATION_JSON)
                .contentType(APPLICATION_JSON))

        // Then it should be persisted and a location returned
        .andExpect(status().isCreated())
        .andExpect(header().string("location", "http://localhost/offer/456"));
    }

    @Test
    public void testGetOffer() throws Exception {
        // Given we can retrieve an offer from the repository
        Offer offer = new Offer("Test Offer", new Money(123.45, getInstance("GBP")));
        offer.setConditions(ImmutableList.of(new Condition("condition 1"), new Condition("condition 2")));
        when(offerRepository.findOne(123L)).thenReturn(offer);

        // When we retrieve an offer by id that exists
        mock.perform(get("/offer/123"))

        // Then expect to receive a sensible response
        .andExpect(status().isOk())
        .andExpect(jsonPath("description", is("Test Offer")))
        .andExpect(jsonPath("conditions[0].description", is("condition 1")))
        .andExpect(jsonPath("price.amount", is(123.45)))
        .andExpect(jsonPath("price.currency", is("GBP")));
    }

    @Test
    public void testGetNonExistentOffer() throws Exception {
        // Given we can retrieve an offer from the repository
        when(offerRepository.findOne(123L)).thenReturn(null);

        // When we retrieve an offer by id that exists
        mock.perform(get("/offer/123"))

        // Then expect to receive a non-found (404) response
        .andExpect(status().isNotFound());
    }
}