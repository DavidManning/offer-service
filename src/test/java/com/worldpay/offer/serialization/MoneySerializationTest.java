package com.worldpay.offer.serialization;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.worldpay.offer.domain.Money;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Currency;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class MoneySerializationTest {

    @Test
    public void serialize() throws IOException {
        MoneySerialization.MoneySerializer serializer = new MoneySerialization.MoneySerializer();
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        JsonGenerator jsonGenerator = new JsonFactory().createGenerator(output);
        jsonGenerator.writeStartObject();

        serializer.serializeObject(new Money(12345.674567876, Currency.getInstance("GBP")), jsonGenerator,null);
        jsonGenerator.close();

        assertThat(output.toString(), is("{\"amount\":12345.67,\"currency\":\"GBP\"}"));
    }

    @Test
    public void deserialize() throws IOException {
        MoneySerialization.MoneyDeserializer deserializer = new MoneySerialization.MoneyDeserializer();
        String serialized = "{\"amount\":12345.6789,\"currency\":\"GBP\"}";

        Money money = deserializer.deserializeObject(null, null, null, new ObjectMapper().readTree(serialized));
        assertThat(money.getAmount(), is(12345.68));
        assertThat(money.getCurrency(), is(Currency.getInstance("GBP")));
    }

}