package com.worldpay.offer.controller;

import com.worldpay.offer.domain.Offer;
import com.worldpay.offer.domain.OfferRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;

import static java.lang.String.format;
import static org.springframework.http.HttpStatus.NOT_FOUND;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

/**
 * Handles requests on the /offer resource.
 */
@Controller
@RequestMapping("/offer")
public class OfferController {

    private static final Logger LOG = LoggerFactory.getLogger(OfferController.class);

    @Autowired
    private OfferRepository offerRepository;

    /**
     * Creates a new Offer, returning its location as a header for future retrieval.
     */
    @RequestMapping(method = POST)
    public @ResponseBody ResponseEntity createOffer(@Valid @RequestBody Offer offer) {
        LOG.info("Creating offer");
        // Create the entity and obtain primary key
        offer = offerRepository.save(offer);

        // Add location header to response
        URI location = MvcUriComponentsBuilder.fromMethodName(OfferController.class, "getOfferById", offer.getId())
                .buildAndExpand(offer.getId()).toUri();

        return ResponseEntity.created(location).build();
    }

    /**
     * Retrieves an Offer identified by the supplied id.
     */
    @RequestMapping(method = GET, value = "/{id}")
    public @ResponseBody Offer getOfferById(@PathVariable("id") long id) {
        LOG.info(format("Retrieving offer with ID [%d]", id));

        Offer offer = offerRepository.findOne(id);
        if (offer == null) {
            throw new NotFoundException();
        }
        return offer;
    }

    /**
     * Indicates that a resource cannot be found at a given URL.
     */
    @ResponseStatus(value = NOT_FOUND)
    public static final class NotFoundException extends RuntimeException {}
}
