package com.worldpay.offer.domain;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

/**
 * Provides CRUD operations on Offers.
 */
@Service
public interface OfferRepository extends CrudRepository<Offer, Long> {

}
