package com.worldpay.offer.domain;

import com.google.common.base.Preconditions;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.Currency;

/**
 * Represents a monetary amount and currency.
 */
@Embeddable
public class Money {

    @Min(0)
    private double amount;

    @NotNull
    private Currency currency;

    /**
     * Constructor for JPA/Hibernate's benefit. Not to be used directly.
     */
    protected Money() {}

    public Money(double amount, Currency currency) {
        this.amount = amount;
        this.currency = currency;
    }

    public Currency getCurrency() {
        return currency;
    }

    public double getAmount() {
        return amount;
    }
}
