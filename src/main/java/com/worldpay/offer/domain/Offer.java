package com.worldpay.offer.domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Represents an offer to sell a product at a defined price with optional conditions attached.
 */
@Entity
public class Offer {
    @Id
    @GeneratedValue
    private long id;

    @Column(nullable = false)
    @NotNull
    private String description;

    @OneToMany(cascade = CascadeType.ALL)
    private List<Condition> conditions;

    @Embedded
    private Money price;

    /**
     * Constructor for JPA/Hibernate's benefit. Not to be used directly.
     */
    protected Offer() {}

    @JsonCreator
    public Offer(@JsonProperty("description") String description, @JsonProperty("price") Money price) {
        this.description = description;
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Money getPrice() {
        return price;
    }

    public void setPrice(Money price) {
        this.price = price;
    }

    public Object getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public List<Condition> getConditions() {
        return conditions;
    }

    public void setConditions(List<Condition> conditions) {
        this.conditions = conditions;
    }
}
