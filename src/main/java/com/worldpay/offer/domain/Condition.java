package com.worldpay.offer.domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Condition {

    @Id
    @GeneratedValue
    private long id;

    private String description;

    /**
     * Constructor for JPA/Hibernate's benefit. Not to be used directly.
     */
    protected Condition() {}

    @JsonCreator
    public Condition(@JsonProperty String description) {
        this.description = description;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
