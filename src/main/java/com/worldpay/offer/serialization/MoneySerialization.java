package com.worldpay.offer.serialization;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.worldpay.offer.domain.Money;
import org.apache.commons.math3.util.Precision;
import org.springframework.boot.jackson.JsonComponent;
import org.springframework.boot.jackson.JsonObjectDeserializer;
import org.springframework.boot.jackson.JsonObjectSerializer;

import java.io.IOException;
import java.util.Currency;

/**
 * Serializer and Deserializer mechanism for Money objects which additionally handles rounding.
 */
@JsonComponent
public class MoneySerialization {

    public static class MoneySerializer extends JsonObjectSerializer<Money> {

        @Override
        protected void serializeObject(Money money, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
            double roundedAmount = Precision.round(money.getAmount(), 2);
            jsonGenerator.writeNumberField("amount", roundedAmount);
            jsonGenerator.writeStringField("currency", money.getCurrency().getCurrencyCode());
        }
    }

    public static class MoneyDeserializer extends JsonObjectDeserializer<Money> {

        @Override
        protected Money deserializeObject(JsonParser jsonParser, DeserializationContext context, ObjectCodec objectCodec, JsonNode jsonNode) throws IOException {
            double amount = jsonNode.get("amount").asDouble();
            Currency currency = Currency.getInstance(jsonNode.get("currency").asText());
            return new Money(Precision.round(amount, 2), currency);
        }
    }
}
